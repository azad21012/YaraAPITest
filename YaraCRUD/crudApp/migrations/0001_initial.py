# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ShoppingList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('good', models.CharField(max_length=150)),
                ('goodID', models.IntegerField()),
                ('timeCreated', models.DateTimeField(auto_now=True)),
                ('timeUpdate', models.DateTimeField(auto_now_add=True)),
                ('number', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='TestModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=150)),
                ('address', models.TextField()),
                ('userID', models.IntegerField()),
                ('phone_number', models.CharField(validators=[django.core.validators.RegexValidator(message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.", regex='^\\+?1?\\d{9,15}$')], max_length=17)),
                ('email', models.EmailField(error_messages={'required': 'Please provide your email address.', 'unique': 'An account with this email exist.'}, max_length=254)),
                ('purchase', models.CharField(max_length=150)),
                ('purchaseID', models.IntegerField()),
                ('timeCreated', models.DateTimeField(auto_now=True)),
                ('timeUpdated', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
