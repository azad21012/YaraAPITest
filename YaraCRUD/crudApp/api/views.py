from rest_framework import generics
from .serializers import *
from rest_framework.permissions import (AllowAny, IsAuthenticated, IsAdminUser)

# from rest_framework.throttling import UserRateThrottle


class TesAPICreateSerializer(generics.CreateAPIView):
    queryset = TestModel.objects.all()
    serializer_class = ModelCreateSerializer
    permission_classes = [IsAuthenticated]


class TesAPIListSerializer(generics.ListAPIView):
    queryset = TestModel.objects.all()
    serializer_class = ModelListSerializer
    permission_classes = [AllowAny]


class TesAPIDetailSerializer(generics.RetrieveAPIView):
    lookup_field = 'id'
    queryset = TestModel.objects.all()
    serializer_class = ModelDetailSerializer
    # throttle_classes = (UserRateThrottle)


class TesAPIUpdateSerializer(generics.RetrieveUpdateAPIView):
    lookup_field = 'id'
    queryset = TestModel.objects.all()
    serializer_class = ModelEditSerializer
    permission_classes = [IsAdminUser]


class TesAPIDeleteSerializer(generics.DestroyAPIView):
    lookup_field = 'id'
    queryset = TestModel.objects.all()
    serializer_class = ModelDeleteSerializer
    permission_classes = [IsAdminUser]
