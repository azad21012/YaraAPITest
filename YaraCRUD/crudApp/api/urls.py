from django.conf.urls import url
from . import views

urlpatterns = [
    # Examples:
    url(r'^api/create/$', views.TesAPICreateSerializer.as_view()),
    url(r'^api/list/$', views.TesAPIListSerializer.as_view()),
    url(r'^api/detail/(?P<id>[0-9])/$', views.TesAPIDetailSerializer.as_view()),
    url(r'^api/edit/(?P<id>[0-9])/$', views.TesAPIUpdateSerializer.as_view()),
    url(r'^api/delete/(?P<id>[0-9])/$', views.TesAPIDeleteSerializer.as_view()),
]