from rest_framework.serializers import ModelSerializer
from crudApp.models import TestModel


class ModelCreateSerializer(ModelSerializer):

    class Meta:
        model = TestModel
        fields = '__all__'


class ModelListSerializer(ModelSerializer):

    class Meta:
        model = TestModel
        fields = ('id', 'name')


class ModelDetailSerializer(ModelSerializer):

    class Meta:
        model = TestModel
        fields = '__all__'


class ModelEditSerializer(ModelSerializer):

    class Meta:
        model = TestModel
        fields = '__all__'


class ModelDeleteSerializer(ModelSerializer):

    class Meta:
        model = TestModel
        fields = '__all__'