from django.db import models
from django.core.validators import RegexValidator
from django.contrib.auth.models import User

# Create your models here.


# this class can be used in furure for creating shopping list . with foreinkey we can relate tables
class ShoppingList(models.Model):
    good = models.CharField(max_length=150)
    goodID = models.IntegerField(blank=False)
    timeCreated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timeUpdate = models.DateTimeField(auto_now=False, auto_now_add=True)
    number = models.IntegerField(blank=False)

# our base model for this project


class TestModel(models.Model):
    name = models.CharField(max_length=150)
    address = models.TextField()
    userID = models.IntegerField(blank=False)

    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format:"
                                 " '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex], max_length=20,)  # validators should be a list

    email = models.EmailField(max_length=150, blank=False, unique=False,
                              error_messages={'required': 'Please provide your email address.',
                                              'unique': 'An account with this email exist.'}, )

    purchase = models.CharField(max_length=150)
    purchaseID = models.IntegerField(blank=False)
    timeCreated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timeUpdated = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return self.name